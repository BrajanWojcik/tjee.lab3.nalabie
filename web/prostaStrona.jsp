<%-- 
    Document   : prostaStrona
    Created on : 2022-05-27, 12:07:12
    Author     : student
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
           String dana = request.getParameter("cena");
        %>
        <jsp:useBean id="dBean" class="Lab3.DataBean" scope="session"></jsp:useBean>
        <jsp:setProperty name="dBean" property="przykladowaDana" value="<%=dana%>" />
        Zapisałem dane do Beana.<br>
        Wyprowadzam dane z Beana:
        <i><jsp:getProperty name="dBean" property="przykladowaDana" /></i><br>
        Wywołuję inną metodą Beana:
        <i><%= dBean.Dopisz("zł")%></i>
        <meta http-equiv="refresh" content="5; URL=index.html">
    </body>
</html>
